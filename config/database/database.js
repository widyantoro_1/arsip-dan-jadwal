const Sequelize = require('sequelize')
require('dotenv').config()
const db = new Sequelize(
  process.env.DATABASE_PG,
  process.env.DATABASE_PG_USERNAME,
  process.env.DATABASE_PG_PASSWORD,
  {
    dialect: 'postgres',
    host: 'localhost'
  }
)

module.exports = db;