const serviceDataArsip = {}

const model = require('../model/model')
const db = require('../config/database/database')
const arsipQuery = require('../model/raw-query/arsip-query')

// POST
serviceDataArsip.doCreateArsip = async(data)=>{
    let dataArsip = await model.tdDataArsip.create(data);
    return dataArsip;
}
serviceDataArsip.doCreateArsipDetail = async(data)=>{
    let dataArsipDetail = await model.tdDataArsipDetail.create(data);
    return dataArsipDetail;
}
serviceDataArsip.doCreateArsipAndDetail = async(data)=>{
    let dataArsip = data.dataArsip
    let dataArsipDetail = data.dataArsipDetail
    await serviceDataArsip.doCreateArsip(dataArsip)
    dataArsipDetail.forEach(async(element) => {
        await serviceDataArsip.doCreateArsipDetail(element)
    });
    return data
}
// GET
serviceDataArsip.getAllDataArsip = async()=>{
    let result = await model.tdDataArsip.findAll()
    return result
}
serviceDataArsip.getDataArsipByIdData = async(id_data)=>{
    console.log(id_data)
    let result = await db.query(arsipQuery.getDataArsipbyIdData,{
        replacements : {
            id_data : id_data
        }
    })
    return result
}

serviceDataArsip.getDataDetailArsipByIdData = async(id_data)=>{
    let result = await db.query(arsipQuery.getDataDetailbyIdData,{
        replacements:{
            id_data:id_data
        }
    })
    return result
}


module.exports=serviceDataArsip