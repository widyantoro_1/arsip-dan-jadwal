const serviceUser = {}

const model = require('../model/model')
const userQuery = require('../model/raw-query/user-query')
const db = require('../config/database/database')

serviceUser.doCreateUser = async(data)=>{
    let postDataUser = await model.tdUser.create(data);
    return postDataUser;
}

serviceUser.doUpdateSingleUser = async(data)=>{
    let id_user = data.id_user
    await model.tdUser.update(
        {nama_user : data.nama_user,
         umur : data.umur,
         alamat : data.alamat,
         email : data.email},
         {
            where : {
            id_user :id_user
        }
    });
    return data;
}
// GET
serviceUser.getAllUser = async () =>{
    let result = await model.tdUser.findAll()
    return result
}

serviceUser.getUser = async (params) =>{
    let Query = userQuery.getUser
    let replacement = {replacements:{

    }}
    let and = ``
    let where = ` where `
    if (params.id_user !=null) {
        Query = Query + where+ and+ `id_user =:id_user`
        replacement.replacements.id_user=params.id_user
        where = ``
        and = ` and `
    }
    if (params.nama_user !=null) {
        Query = Query +where+ and+ `nama_user =:nama_user`
        console.log(Query)
        replacement.replacements.nama_user=params.nama_user
        where = ``
        and = ` and `
    }
    if (params.umur !=null) {
        Query = Query +where+ and+ `umur =:umur`
        console.log(Query)
        replacement.replacements.umur=params.umur
    }
    console.log(Query)
    let result = await db.query(Query,replacement)
    return result
}

module.exports=serviceUser