
const database = require('../config/database/database');
const serviceUser = require('../service/service-user')
const status = require('../helpers/status');
const controllerUser = {}

// POST
controllerUser.PostSingleUser = async(req, res)=>{
    try{
        let data = req.body;
        let result = await serviceUser.doCreateUser(data);
        res.status(status.statusCode.success).json(status.successMessage(result));
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}

controllerUser.UpdateSingleUser = async(req, res)=>{
    try{
        let data = req.body;
        let result = await serviceUser.doUpdateSingleUser(data);
        res.status(status.statusCode.success).json(status.successMessage(result));
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}
// GET

controllerUser.getAllUser = async(req, res)=>{
    try{
        let result = await serviceUser.getAllUser();
        res.status(status.statusCode.success).json(status.successMessage(result));
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}
controllerUser.getUserByIdOrName = async(req, res)=>{
    try{
        const{id_user,nama_user, umur} = req.query
        const data = {
            id_user : id_user,
            nama_user: nama_user,
            umur : umur
        }
        let result = await serviceUser.getUser(data);
        res.status(status.statusCode.success).json(status.successMessage(result[0]));
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}
module.exports =controllerUser


