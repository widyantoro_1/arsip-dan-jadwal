
const database = require('../config/database/database');
const serviceUser = require('../service/service-user')
const arsipService = require('../service/service-arsip-data')
const status = require('../helpers/status');
const json2xml = require('jsontoxml');
// const xml2json = require('xml2json');
const controllerArsip = {}


// POST
controllerArsip.PostDataArsip = async(req, res)=>{
    try{
        let data = req.body;
        let result = await arsipService.doCreateArsip(data);
        res.status(status.statusCode.success).json(status.successMessage(result));
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}
controllerArsip.PostDataArsipDetail = async(req, res)=>{
    try{
        const id_data_arsip = req.body.id_data
        const data_detail = req.body.dataDetailList
        let jumlahProses = 0
        let dataResult=[]
        await data_detail.forEach(async(dataDetail) => {
                let data = {id_data:id_data_arsip,
                    nama_barang:dataDetail.nama_barang,
                    jumlah:dataDetail.jumlah,
                    keterangan:dataDetail.keterangan}
                let result = await arsipService.doCreateArsipDetail(data);
                jumlahProses++
                dataResult.push(result.dataValues)
                if(data_detail.length === jumlahProses){
                    res.status(status.statusCode.success).json(status.successMessage(dataResult));
                }
        });
        
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}

controllerArsip.getAllDataArsip = async(req, res)=>{
    try{
        let result = await arsipService.getAllDataArsip()
       
        res.status(status.statusCode.success).json(status.successMessage(result));
        
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}

controllerArsip.getAllDataArsipDanDetail = async(req, res)=>{
    try{
        const id_data = req.params.id_data
        let dataArsip = await arsipService.getDataArsipByIdData(id_data)
        let dataArsipDetail = await arsipService.getDataDetailArsipByIdData(id_data)
        // console.log(dataArsipDetail)
        let result ={id_data : dataArsip[0][0].id_data,
                     nama_barang : dataArsip[0][0].nama_barang||"",
                     jumlah : dataArsip[0][0].jumlah,
                     keterangan : dataArsip[0][0].keterangan,
                     dataArsipDetail : dataArsipDetail[0]
                    }
        let xml = json2xml(result,{prettyPrint: true})
       
        console.log(xml)
        
        res.status(status.statusCode.success).json(status.successMessage(result));
        
    }catch(err){
        res.status(status.statusCode.bad).json(status.errorMessage(err.message));
    }
}
module.exports =controllerArsip


