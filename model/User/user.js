const Sequelize = require('sequelize')
const db=require('../../config/database/database')

const user = {}


user.td_user = db.define('td_user', {
    id_user: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      field: 'id_user',
      primaryKey: true
    },
    nama_user: {
      type: Sequelize.CHAR,
      field: 'nama_user'
    },
    umur: {
      type: Sequelize.INTEGER,
      field: 'umur'
    },
    email: {
      type: Sequelize.CHAR,
      field: 'email'
    },
    alamat: {
      type: Sequelize.CHAR,
      field: 'alamat'
    }

  }, {
    schema: 'user',
    freezeTableName: true,
    timestamps: false
  });

  user.td_user_login = db.define('td_user', {
    id_user: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      field: 'id_user_login',
      primaryKey: true
    },
    username: {
      type: Sequelize.CHAR,
      field: 'username'
    },
    password: {
      type: Sequelize.CHAR,
      field: 'password'
    }
  }, {
    schema: 'user',
    freezeTableName: true,
    timestamps: false
  });

  module.exports= user;