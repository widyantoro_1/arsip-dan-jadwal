const Sequelize = require('sequelize')
const db=require('../../config/database/database')

const histori = {}



histori.td_histori= db.define('td_histori', {
    id_histori: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      field: 'id_histori',
      primaryKey: true
    },
    id_user_admin: {
      type: Sequelize.CHAR,
      field: 'id_user_admin'
    },
    keterangan: {
      type: Sequelize.CHAR,
      field: 'keterangan'
    },
    waktu_rekam: {
      type: Sequelize.DATE,
      field: 'waktu_rekam'
    }

  }, {
    schema: 'user',
    freezeTableName: true,
    timestamps: false
  });

  module.exports= histori;