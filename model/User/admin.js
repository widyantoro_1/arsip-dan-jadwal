const Sequelize = require('sequelize')
const db=require('../../config/database/database')

const admin = {}


admin.td_admiin = db.define('td_user', {
    id_admin: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      field: 'id_user_login',
      primaryKey: true
    },
    username: {
      type: Sequelize.CHAR,
      field: 'username'
    },
    password: {
      type: Sequelize.CHAR,
      field: 'password'
    }
  }, {
    schema: 'user',
    freezeTableName: true,
    timestamps: false
  });

  module.exports = admin