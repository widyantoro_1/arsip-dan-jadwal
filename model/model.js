const model = {}

const user = require('./user/user')

const histori = require('./user/histori')

const arsipData = require('./data/data-arsip')



// User
model.tdUser = user.td_user
model.tdHistori = histori.td_histori
// Arsip
model.tdDataArsip = arsipData.td_data_arsip
model.tdDataArsipDetail = arsipData.td_data_arsip_detail

module.exports=model