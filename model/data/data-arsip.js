const Sequelize = require('sequelize')
const db=require('../../config/database/database')

const dataArsip = {}


dataArsip.td_data_arsip = db.define('td_data_arsip', {
    id_data: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      field: 'id_data',
      primaryKey: true
    },
    jenis_barang: {
      type: Sequelize.CHAR,
      field: 'jenis_barang'
    },
    jumlah: {
      type: Sequelize.INTEGER,
      field: 'jumlah'
    },
    keterangan: {
      type: Sequelize.CHAR,
      field: 'keterangan'
    }

  }, {
    schema: 'data',
    freezeTableName: true,
    timestamps: false
  });

  dataArsip.td_data_arsip_detail = db.define('td_data_arsip_detail', {
    id_detail: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      field: 'id_detail',
      primaryKey: true
    },
    id_data: {
        type: Sequelize.CHAR,
        field: 'id_data'
      },
    nama_barang: {
      type: Sequelize.CHAR,
      field: 'nama_barang'
    },
    jumlah: {
        type: Sequelize.INTEGER,
        field: 'jumlah'
      }
    ,
    keterangan: {
        type: Sequelize.CHAR,
        field: 'keterangan'
      }
    ,
  }, {
    schema: 'data',
    freezeTableName: true,
    timestamps: false
  });
  module.exports= dataArsip;