const router = require('express').Router()

const userController = require('../controller/user-controller')
const arsipController = require('../controller/arsip-controller')





//GET
router.get('/get-all-user', userController.getAllUser)
router.get('/user-by-id-or-name-or-umur',userController.getUserByIdOrName)
router.get('/get-all-data-arsip',arsipController.getAllDataArsip)
router.get('/get-all-data-arsip-dan-detail/:id_data',arsipController.getAllDataArsipDanDetail)

// POST
console.log('test')
router.post('/post-single-user',userController.PostSingleUser)
router.post('/post-data-arsip',arsipController.PostDataArsip)
router.post('/post-data-arsip-detail',arsipController.PostDataArsipDetail)

//UPDATE
router.put('/update-single-user',userController.UpdateSingleUser)



module.exports=router