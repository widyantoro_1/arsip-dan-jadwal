const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app =express()
const router = require('./routes/arsip-routes')

// PORT
const port = process.env.APP_PORT || 9000
app.listen(port).on('listening', () => {
    console.log(`API server started on port : ${port}`)
  })

app.use(cors()) 
app.use(bodyParser.urlencoded({extended:false})) 
app.use(bodyParser.json()) 
// tes
app.get('/test',(req,res)=>{
    res.status(200).json({
      code: '01',
      message: 'Test Successs'
    })
  })
// Router arsip
app.use("/api/arsip/",router)
console.log("test")

module.exports=app